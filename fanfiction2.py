import requests
import re
from reportlab.platypus import SimpleDocTemplate, Paragraph, Spacer, PageBreak
from reportlab.lib.pagesizes import A4 
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle

from reportlab.lib.fonts import addMapping
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.lib.enums import TA_LEFT, TA_CENTER, TA_RIGHT, TA_JUSTIFY 

styles = getSampleStyleSheet()
pdfmetrics.registerFont(TTFont('Arial', 'Arial.ttf'))
addMapping('Arial', 1,1, 'Arial')

styles = getSampleStyleSheet()
#slogi, v katerih bo izpisana zgodba
styles.add(ParagraphStyle(name = 'Naslov',
                          alignment=TA_CENTER,
                          fontName = 'Arial',
                          fontSize = 30,                          
                          leading= 30))
styles.add(ParagraphStyle(name = 'Avtor',
                          fontName = 'Arial',
                          fontSize = 20,                          
                          leading= 20))
styles.add(ParagraphStyle(name = 'NaslovPoglavja',
                          fontName = 'Arial',
                          fontSize = 15,                          
                          leading= 20))
styles.add(ParagraphStyle(name = 'Besedilo',
                          fontName = 'Arial',
                          fontSize = 11,                          
                          leading= 11))

#malo oblikovanja strani
def naslednje_strani(canvas, doc):
    canvas.saveState()
    canvas.setFont('Arial',10)
    canvas.drawCentredString(A4[0]//2, 20, "%d" % (doc.page-1))
    canvas.restoreState()
    
def glava(url):
    #v glavi so zapisani skoraj vsi bistveni podatki o zgodbi
    #(pomožna)funkcija bo prejela veljaven(!) url poglavja in vrnila glavo
    stran = requests.get(url)
    izraz = re.findall(r"<div id=profile_top style=\'min-height:112px;\'>(.*?)<div align=center",
               stran.text, flags=re.DOTALL)
    return izraz[0]

def podatki(url):
    #funkcija, ki prejme url in zgodbe in vrne seznam s sinopsisom in nekaj drugimi podatki
    tekst = glava(url)
    s = []
    #sinopsis
    n = re.findall(r'<div style=\'margin-top:2px\' class=\'xcontrast_txt\'>(.*?)</div>',
                   tekst, flags=re.DOTALL)
    if n!= []:
        s.append(n[0])
    else:
        s.append('')    
    #jezik
    j = re.findall(r'</a> - (.*?) -', tekst, flags=re.DOTALL)
    if j!= []:
        s.append(j[0])
    else:
        s.append('')
    #stevilo_besed
    b = re.findall(r'Words: (.*?) -', tekst, flags=re.DOTALL)
    if b!= []:
        s.append(b[0])
    else:
        s.append('')
    #zakljucenost
    z = re.findall(r'Status: Complete', tekst, flags=re.DOTALL)
    s.append(z != [])
    return s


def naslov(url):
    #funkcija, ki bo sprejela veljaven(!) url enega poglavja neke zgodbe in vrnila njen naslov
    tekst = glava(url)
    n = re.findall(r'Follow/Fav</button><b class=\'xcontrast_txt\'>(.*?)</b>',
                   tekst, flags=re.DOTALL)
    return n[0]

def avtor(url):
    #funkcija, ki bo sprejela veljaven(!) url enega poglavja neke zgodbe in vrnila vzdevek njenega avtorja
    tekst = glava(url)
    n = re.findall(r'>(.*?)</a>',tekst, flags=re.DOTALL)
    k = re.search(r'/(.*?)\'>\1', n[0], flags=re.DOTALL)
    if k != None:
        return k.group(1)
    else:
        n = re.findall(r'<a class=\'xcontrast_txt\' href=\'/u/(\d)+/(.*?)\'>(.*?)</a> <span class=\'icon-mail-1  xcontrast_txt\' >',
                       tekst, flags=re.DOTALL)
        return n[0][2]


def naslovi_poglavij(url):
    #funkcija, ki bo sprejela veljaven(!) url enega poglavja neke zgodbe in vrnila naslove vseh njenih poglavij - za kazala in začetke poglavij!
    #če je poglavje samo eno, funkcija vrne prazen seznam
    stran = requests.get(url)
    izraz = re.findall(r"<SELECT(.*?)/select>", stran.text, flags=re.DOTALL)
    if izraz != []:
        seznam = re.findall(r"option  value=(\d+) (selected)?>(.*?)<", izraz[0], flags=re.DOTALL)
        return [i[2] for i in seznam]
    return []

def stevilo_poglavij(url):
    a = len(naslovi_poglavij(url))
    return a if a != 0 else 1

def izpisi_zgodbo(url):
    #funkcija, ki bo sprejela veljaven(!) url 1. poglavja zgodbe in izpisala celo zgodbo v pdf datoteko
    id = re.findall(r's/([0-9]*)/', url)
    n2 = re.findall('/([^/]*?)/?$', url)
    niz = n2[0]  + '.pdf'
    doc = SimpleDocTemplate(niz, pagesize=A4,
                            rightMargin=72, leftMargin=73,
                            topMargin=72, bottomMargin=36)
    story = []
    story.append(Spacer(1, 20))
    story.append(Paragraph(naslov(url), styles['Naslov']))
    story.append(Spacer(1, 30))
    story.append(Paragraph('By: ' + avtor(url),styles['Avtor']))
    story.append(Spacer(1, 20))
    story.append(Paragraph('Link: <font color=blue> ' + url + '</font>',styles['Besedilo']))
    story.append(PageBreak())
    if stevilo_poglavij(url) == 1:
        #zgodbe z enim poglavjem
        stran = requests.get(url)
        izraz = re.findall(r"<div class='storytext xcontrast_txt nocopy' id='storytext'>(.*?)</div>",
                      stran.text, flags=re.DOTALL|re.IGNORECASE)
        if (not '</p>' in izraz[0]) and (not'</P>' in izraz[0]):    
        #če je html neobičajen v smislu, da odstavkov ne ločuje s <p> in </p>
            odstavki = re.split('(<br[ /]*>)(\s*<br[ /]*>)+', izraz[0], flags = re.IGNORECASE)
            for odstavek in odstavki:
                if odstavek != '<br>' and odstavek != '<\br>' and odstavek !=  '<BR>' and odstavek != '</BR>':
                    odstavek = re.sub('<(.*?)>' ,'' , odstavek, flags=re.DOTALL)
                    story.append(Paragraph(odstavek, styles['Besedilo']))
                    story.append(Spacer(1, 12))                       
        tekst = izraz[0] + '</p>'
        tekst2 = re.sub(r'<hr(.*?)>', '<p>_</p>', tekst)
        odstavki = re.findall(r'<p(.*?)>(.*?)</p>', tekst2,
                              flags = re.DOTALL|re.IGNORECASE)
        odstavki = [i[1] for i in odstavki]   
        for odstavek in odstavki:
            odstavek = re.sub('<(.*?)>' ,'' , odstavek, flags=re.DOTALL)
            story.append(Paragraph(odstavek, styles['Besedilo']))
            story.append(Spacer(1, 12))
    else:
        #zgodbe z več poglavji
        for i in range(stevilo_poglavij(url)):
            m = naslovi_poglavij(url)[i]
            story.append(Paragraph( m, styles['NaslovPoglavja']))
            story.append(Spacer(1, 12))
            url1 = 'https://www.fanfiction.net/s/' + id[0] + '/' + str(i+1) + '/' + n2[0]
            stran = requests.get(url1)
            izraz = re.findall(r"<div class='storytext xcontrast_txt nocopy' id='storytext'>(.*?)</div>",
                               stran.text, flags = re.DOTALL|re.IGNORECASE)
            if (not '</p>' in izraz[0]) and (not'</P>' in izraz[0]):
            #če je html neobičajen v smislu, da odstavkov ne ločuje s <p> in </p>
                odstavki = re.split('(<br[ /]*>)(\s*<br[ /]*>)+', izraz[0], flags=re.IGNORECASE)
                for odstavek in odstavki:
                    if odstavek != '<br>' and odstavek != '<\br>' and odstavek !=  '<BR>' and odstavek != '</BR>':
                        odstavek = re.sub('<(.*?)>' ,'' , odstavek, flags=re.DOTALL)
                        story.append(Paragraph(odstavek, styles['Besedilo']))
                        story.append(Spacer(1, 12)) 
            tekst = izraz[0] + '</p>'
            tekst2 = re.sub(r'<hr(.*?)>', '<p>_</p>', tekst)
            odstavki = re.findall(r'<p(.*?)>(.*?)</p>', tekst2,
                              flags = re.DOTALL|re.IGNORECASE)
            odstavki = [i[1] for i in odstavki]
            for odstavek in odstavki:
                odstavek = re.sub('<(.*?)>' ,'' , odstavek, flags=re.DOTALL)
                story.append(Paragraph(odstavek, styles['Besedilo']))
                story.append(Spacer(1, 12))
            story.append(PageBreak())
    doc.build(story, onLaterPages=naslednje_strani)
