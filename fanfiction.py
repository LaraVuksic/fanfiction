from tkinter import *
from tkinter import ttk
import requests
import re
import fanfiction2

url="https://www.fanfiction.net"
seznam=[]

glavno_okno = Tk()

############### 1. okno ################

#funkcija, ki bo prehajala med posameznimi okvirji
def prehod(okvir1,okvir2):
    okvir1.pack_forget()
    okvir2.pack()
    if okvir2 == f2:
        listbox.select_set(0)
    
#poišče možna dela za 2. stran, naredi globalen slovar
def mozna_dela():
    global slovar
    stran = requests.get(url+"/"+v1.get()+"/")
    naslovi = re.findall(r'" title="(.*?)">.*?</a>',\
                       stran.text, flags=re.DOTALL)
    povezave=re.findall(r'<div><a href="(.*?)" title=',stran.text,flags=re.DOTALL)
    slovar={naslovi[i]:povezave[i] for i in range(len(naslovi))}

#funkcija, na 2. strani
def izbira_dela(niz):
    s=sorted([i for i in slovar.keys() if niz.lower() in i.lower()])
    scrollbar = Scrollbar(f2, orient=VERTICAL,width=15)
    scrollbar.grid(column=7,row=2,sticky=S+N)
    global listbox
    listbox = Listbox(f2,height=20,width=60,yscrollcommand = scrollbar.set,selectmode='SINGLE')
    listbox.grid(row=2,columnspan=6,sticky=W)
    for item in s:
        listbox.insert(END,item)
    scrollbar.config( command = listbox.yview )
    listbox.select_set(0)

#ponudi možna dela na 2. strani:
def prehod1():
    mozna_dela()
    izbira_dela("")
    prehod(f1,f2)

f1 = Frame(glavno_okno,height=40, width=75)
Label(f1,text = "FanFiction",font=("Helvetica", 16),width = 20,height=4).grid()
v1 = StringVar()
v1.set("book")
Radiobutton(f1, text="Knjige", variable=v1, value="book",indicatoron=0,width=50).grid(sticky=W)
Radiobutton(f1, text="Filmi", variable=v1, value="movie",indicatoron=0,width=50).grid(sticky=W)
Radiobutton(f1, text="Predstave/muzikali", variable=v1, value="play",indicatoron=0,width=50).grid(sticky=W)
Radiobutton(f1, text="Risanke", variable=v1, value="cartoon",indicatoron=0,width=50).grid(sticky=W)
Radiobutton(f1, text="TV oddaje", variable=v1, value="tv",indicatoron=0,width=50).grid(sticky=W)
Radiobutton(f1, text="Igre", variable=v1, value="game",indicatoron=0,width=50).grid(sticky=W)
Radiobutton(f1, text="Stripi", variable=v1, value="comic",indicatoron=0,width=50).grid(sticky=W)
Radiobutton(f1, text="Mange", variable=v1, value="anime",indicatoron=0,width=50).grid(sticky=W)
Radiobutton(f1, text="Drugo", variable=v1, value="misc",indicatoron=0,width=50).grid(sticky=W)
gumb1=Button(f1,text = "Naprej",command = prehod1,background="#C3D5ED",borderwidth=3).grid(ipady=2,ipadx=20,padx=30,pady=10)


################# 3. okno ####################

#poišče možne kriterije
def mozni_kriteriji(url):
    stran=requests.get(url)
    izsek=re.findall(r"Name=sortid .*?</SELECT>",stran.text,flags=re.DOTALL)
    kriterij = re.findall(r"<option value='\d' >(.*?)\n", izsek[0],flags=re.DOTALL)
    povezava = re.findall(r"<option value='(\d)' >.*?\n",izsek[0], flags=re.DOTALL)
    global slovark
    slovark={kriterij[i]:povezava[i] for i in range(len(kriterij))}
    slovark.update({"All":""})
    sk=sorted(slovark)
    return sk
    
#poišče možne jezike
def mozni_jeziki(url):
    stran=requests.get(url)
    izsek=re.findall(r"Name='languageid'.*?</SELECT>",stran.text,flags=re.DOTALL)
    jezik = re.findall(r"option value='\d*' >(.*?)<", izsek[0],flags=re.DOTALL)
    povezava = re.findall(r"option value='(\d*)' >.*?<",izsek[0], flags=re.DOTALL)

    global slovarj
    slovarj={jezik[i]:povezava[i] for i in range(len(jezik))}
    slovarj.update({"All":""})
    sj=sorted(slovarj)
    return sj


#poišče možne žanre
def mozni_zanri(url):
    stran=requests.get(url)
    izsek=re.findall(r"Name=genreid1 .*?</SELECT>",stran.text,flags=re.DOTALL)
    zanr = re.findall(r"option value='\d*'>(.*?)<",izsek[0],flags=re.DOTALL)
    povezava = re.findall(r"option value='(\d*)'>.*?<",izsek[0], flags=re.DOTALL)
    global slovarz
    slovarz={zanr[i]:povezava[i] for i in range(len(zanr))}
    slovarz.update({"All":""})
    slovarz.pop("Genre (A): All")
    sz=sorted(slovarz)
    return sz


#vrne slovar, kjer vsakemu naslovu pripada povezava z zgodbo
def naslovi_in_povezave(url):
    stran = requests.get(url)
    naslovi = re.findall(r' width=\d*? height=\d*?>(.*?)</a>',
                         stran.text, flags=re.DOTALL)
    povezave = re.findall(r'<a  class=stitle href="(.*?)"><img class',
                   stran.text, flags=re.DOTALL)
    d = {}
    for i in range(len(naslovi)):
        if i < 9:
            d['0' + str(i+1) + '. ' + naslovi[i]] = 'https://www.fanfiction.net' + povezave[i]
        else:
            d[str(i+1) + '. ' + naslovi[i]] = 'https://www.fanfiction.net' + povezave[i]
    return d

#poišče možna dela za 3. stran, naredi globalen seznam
def mozna_dela_filtri(url, okno):
    global seznam
    stran = requests.get(url)
    s = re.findall(r' width=\d*? height=\d*?>(.*?)</a>',
                         stran.text, flags=re.DOTALL)
    seznam = [str(i+1) + '. ' + s[i] if i >= 9 else '0' + str(i+1) + '. ' + s[i] for i in range(len(s))]
    scrollbar1 = Scrollbar(okno, orient=VERTICAL,width=15)
    scrollbar1.grid(column=2,row=5,sticky=S+N+W)
    global listbox1
    listbox1 = Listbox(okno,height=20,width=60,yscrollcommand = scrollbar1.set,selectmode='SINGLE',selectbackground="#f69909",fg="black")
    listbox1.grid(column=0,row=5,columnspan=2,sticky=E)
    for item in sorted(seznam):
        listbox1.insert(END,item)
    scrollbar1.config( command = listbox1.yview )
    listbox1.select_set(0)

def iskanje_po_filtrih(url,f3,izbrani_kriterij,izbrani_jezik,izbrani_zanr, n):
    url2=url1 + "?"
    if izbrani_kriterij.get()!="" or izbrani_jezik.get()!="" or izbrani_zanr.get()!="":
        if izbrani_kriterij.get()!="":
            stk=slovark.get(izbrani_kriterij.get())
            url2=url2+"&srt="+stk
        if izbrani_jezik.get()!="":
            stl=slovarj.get(izbrani_jezik.get())
            url2=url2+"&lan="+stl
        if izbrani_zanr.get()!="":
            stg=slovarz.get(izbrani_zanr.get())
            url2=url2+"&g1="+stg
    if n == 0:
        stran.set(1)
    else:
        a = stran.get()
        stran.set(a+n)
    url2 += '&r=10&p='+ str(stran.get())
    #tvegajmo:
    url3.set(url2)
    sinopsiszgodbe.set('')
    return mozna_dela_filtri(url2,f3)

sinopsiszgodbe = StringVar()
stran = IntVar()

#url3 se spremeni ob vsaki izbiri dela, na katerem temeljijo zgodbe, vsakem filtriranju in vsaki spremembi strani.
url3 = StringVar()
url3.set(url)

#funkcija, ki ob kliku na naslov zgodbe prikaže njen sinopsis (in nekaj drugih podatkov)
def prikazi_sinopsis():
    idxs = listbox1.curselection()
    if len(idxs) == 1:
        zgodba = listbox1.get(listbox1.curselection())
        povezava = naslovi_in_povezave(url3.get())[zgodba]
        s = fanfiction2.podatki(povezava)
        sin = s[0] + '\n\n' + 'Jezik: ' + s[1] + '\n' + 'Število besed: ' \
              + s[2] + '\n' + 'Število poglavij: ' + str(fanfiction2.stevilo_poglavij(povezava))
        if s[3]:
            sin += '\n\nAvtor je označil zgodbo kot zaključeno.'
        sinopsiszgodbe.set(sin)
    else:
        sinopsiszgodbe.set('')
               
#ponudi filtre na 3. strani:
def prehod2():
    stran.set(1)
    f3 = Frame(glavno_okno,height=30, width=70)
    Label(f3, text = "Izberi jezik").grid(row=2,column=0,padx=10,pady=5)
    Label(f3, text = "Izberi žanr").grid(row=3,column=0,padx=10,pady=5)
    Label(f3,text = "Razvrsti glede na").grid(row=1,column=0,padx=10,pady=5)
    Button(f3, text = "Nazaj", command = lambda : prehod(f3,f2),background="#C3D5ED",borderwidth=3).grid(row=0,column=3,padx=10,ipadx=10,pady=5,sticky=N+E)
    Button(f3, text = "Na začetek", command = lambda : prehod(f3,f1),background="#C3D5ED",borderwidth=3).grid(row=0,column=4,padx=10,ipadx=10,pady=5,sticky=N+E)
    naslov=listbox.get(listbox.curselection())
    v2=slovar.get(naslov)
    global url1
    url1=url+v2
    url3.set(url1 + '?&r=10&p=1')
    #razvrsti glede na
    izbrani_kriterij=StringVar()
    kriteriji=mozni_kriteriji(url1)
    OptionMenu(f3,izbrani_kriterij, *tuple(kriteriji)).grid(row=1,column=1,ipadx=30,padx=10,pady=5)
    #filter za jezike
    izbrani_jezik=StringVar()
    jeziki=mozni_jeziki(url1)
    OptionMenu(f3,izbrani_jezik, *tuple(jeziki)).grid(row=2,column=1,ipadx=30,padx=10,pady=5)
    #filter za žanre
    izbrani_zanr=StringVar()
    zanri=mozni_zanri(url1)
    OptionMenu(f3,izbrani_zanr, *tuple(zanri)).grid(row=3,column=1,ipadx=30,padx=10,pady=5)
    #listbox s ponujenimi deli, preden filtriramo
    mozna_dela_filtri(url3.get(),f3)
    Button(f3, text = "Filtriraj",command=lambda:iskanje_po_filtrih(url1,f3,izbrani_kriterij,izbrani_jezik,izbrani_zanr,0),bg="#24A333").grid(row=0,column=1,ipadx=28)
    #prostor za izpisovanje sinopsisa
    sinopsiszgodbe.set('')
    Button(f3, text = 'Sinopsis', command = prikazi_sinopsis,borderwidth=3,bg="#FFD27F").grid(row=4,column=3,columnspan=2,padx=10,ipadx=10)
    Label(f3, textvariable= sinopsiszgodbe, wraplength=300).grid(row=5,columnspan=2, column=3,padx=20)
    #gumb, ki bo ponudil izpis zgodbe
    dic = naslovi_in_povezave(url3.get())
    Button(f3, text = "Izpiši zgodbo", command=lambda:fanfiction2.izpisi_zgodbo(naslovi_in_povezave(url3.get())\
                                                                                [listbox1.get(listbox1.curselection())]),borderwidth=3,bg="#FFD27F").grid(row=6,column=3,columnspan=2,padx=10,ipadx=10,pady=3)
    #gumba, ki bosta ponudil prejšnjih/naslednjih 0<=n<=25 zgodb glede na enake izbrane kriterije    
    Button(f3, text = "Prejšnje zgodbe",command=lambda:iskanje_po_filtrih(url1,f3,izbrani_kriterij,izbrani_jezik,izbrani_zanr, -1),borderwidth=3).grid(row=6,column=0,sticky=W,padx=2)
    Button(f3, text = "Več zgodb",command=lambda:iskanje_po_filtrih(url1,f3,izbrani_kriterij,izbrani_jezik,izbrani_zanr, 1),borderwidth=3).grid(row=6,column=1,sticky=E)
    prehod(f2,f3)

################# 2. okno ###################

f2 = Frame(glavno_okno)
Label(f2, text = "Ponujena dela:").grid(row=0,column=0)

Button(f2, text = "Nazaj", command = lambda : prehod(f2,f1),background="#C3D5ED",borderwidth=3).grid(row=4,column=0,ipady=2,ipadx=20,padx=30,pady=10)
gumb2 = Button(f2, text = "Naprej", command = prehod2,borderwidth=3,background="#C3D5ED").grid(row=4,column=1,ipady=2,ipadx=20,padx=30,pady=10)

iskalni_niz = StringVar()
Button(f2,text= "Išči",command=lambda:izbira_dela(iskalni_niz.get()),background="white",borderwidth=3).grid(row=1,column=1,ipadx=30)
iscem = Entry(f2, textvariable=iskalni_niz).grid(row=1,column=0,ipadx=20,pady=3)

f1.pack()

glavno_okno.mainloop()
